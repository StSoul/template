const filter1 = document.querySelector('.filter1');
const filter2 = document.querySelector('.filter2');
const filter3 = document.querySelector('.filter3');

let map;
let markersClusterer;
let coords = {
   lat: 50.45,
   lng: 30.50,
   zoom: 12
};

const points = [
   {
      id: 1,
      lat: 50.447533,
      lng: 30.602099,
      filter1: 1,
      filter2: 0,
      filter3: false,
   },
   {
      id: 2,
      lat: 50.442533,
      lng: 30.604099,
      filter1: 0,
      filter2: 0,
      filter3: true,
   },
   {
      id: 3,
      lat: 50.446533,
      lng: 30.603099,
      filter1: 1,
      filter2: 1,
      filter3: true,
   },
   {
      id: 4,
      lat: 50.467533,
      lng: 30.602099,
      filter1: 0,
      filter2: 1,
      filter3: false,
   },
   {
      id: 5,
      lat: 50.437533,
      lng: 30.601099,
      filter1: 1,
      filter2: 0,
      filter3: false,
   },
];

function initMap() {
   return new google.maps.Map(document.getElementById("map"), {
      center: {lat: coords.lat, lng: coords.lng},
      zoom: coords.zoom,
   });
}

function createMarkers(points) {
   return points.map(position => new google.maps.Marker({position}));
}

function renderMarkerClusterer(markers) {
   if (markersClusterer) {
      markersClusterer.clearMarkers();
   }

   markersClusterer = new MarkerClusterer(map, markers, {
         maxZoom: 14,
         gridSize: 80,
         imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
      }
   );
}

filter1.onchange = function () {
   let filteredPoints;

   if (this.checked) {
      filteredPoints = points.filter(point => point.filter1);
   } else {
      filteredPoints = points;
   }

   render(filteredPoints);
};

filter2.onchange = function () {
   let filteredPoints;

   if (this.checked) {
      filteredPoints = points.filter(point => point.filter2);
   } else {
      filteredPoints = points;
   }

   render(filteredPoints);
};

filter3.onchange = function () {
   let filteredPoints;

   if (this.checked) {
      filteredPoints = points.filter(point => point.filter3);
   } else {
      filteredPoints = points;
   }

   render(filteredPoints);
};

function render(points) {
   const markers = createMarkers(points);
   renderMarkerClusterer(markers);
}

function main() {
   map = initMap();
   render(points);
}

main();
